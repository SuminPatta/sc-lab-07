
public class Main {
	
	public static void main(String[] args){
		TheaterManagement t1 = new TheaterManagement();
		View v = new View();
		t1.date("Sunday");
		v.printPrice(t1.buyTicket('A', 3));
		v.printPrice(t1.buyTicket(2, 'A' , 5));
		v.printSeats(t1.emptySeat(1));
		v.printSeats(t1.emptySeat(2));
		v.printPrice(t1.buyTicket(2, 'A' , 5));
		v.printSeats(t1.emptySeat(2));
		v.printPrice(t1.buyTicket(2, 'O' , 5));
		v.printSeats(t1.emptySeat(2));
		v.printSeats(t1.specificPrice(50));
	}
}
