import java.util.ArrayList;

public class TheaterManagement {

	private ArrayList<Seat> seats = new ArrayList<Seat>();
	SeatPrice th1;
	SeatPrice th2;
	SeatPrice th3;
	String chD;

	public int buyTicket(char row, int col) {
		return buyTicket(1, row, col);
	}

	public int buyTicket(int time, char row, int col) {
		int price = 0;
		if (time == 1) {
			if (th1.ticketPrice[((int) row) - 65][col-1] != 0) {
				price = (int) th1.ticketPrice[((int) row) - 65][col-1];
				th1.ticketPrice[((int) row) - 65][col-1] = 0;
			} else {
				price = 0;
			}
		}
		if (time == 2) {
			if (th2.ticketPrice[((int) row) - 65][col-1] != 0) {
				price = (int) th1.ticketPrice[((int) row) - 65][col-1];
				th2.ticketPrice[((int) row) - 65][col-1] = 0;
			} else {
				price = 0;
			}
		}
		if (chD == "Holiday") {
			if (time == 3) {
				if (th3.ticketPrice[((int) row) - 65][col-1] != 0) {
					price = (int) th1.ticketPrice[((int) row) - 65][col-1];
					th3.ticketPrice[((int) row) - 65][col-1] = 0;
				} else {
					price = 0;
				}
			}
		}
		else{
			price = 0;
		}

		return price;

	}

	public double[][] specificPrice(double ticketprice) {
		double[][] seatprice = new double[15][20];
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 20; j++) {
				if (ticketprice == DataSeatPrice.ticketPrices[i][j]) {
					seatprice[i][j] = DataSeatPrice.ticketPrices[i][j];
				} else {
					seatprice[i][j] = 0;
				}
			}
		}
		return seatprice;
	}
	
	public double[][] emptySeat(int time){
		double[][] seatPrice = new double[15][20];
		if(time == 1){
			for (int i = 0; i < 15; i++) {
				for (int j = 0; j < 20; j++) {
						seatPrice[i][j] = th1.ticketPrice[i][j];
			
				}
			}
		}
		else if (time == 2) {
			for (int i = 0; i < 15; i++) {
				for (int j = 0; j < 20; j++) {
						seatPrice[i][j] = th2.ticketPrice[i][j];
				}
			}
		}
		else{
			for (int i = 0; i < 15; i++) {
				for (int j = 0; j < 20; j++) {
						seatPrice[i][j] = th3.ticketPrice[i][j];
				}
			}
		}
		
		return seatPrice;

	}
	
	
	public void date(String day) {
		String d = day.toLowerCase();
		if (d == "monday" || d == "tuesday" || d == "wednesday"
				|| d == "thursday" || d == "friday") {
			th1 = new SeatPrice();
			th2 = new SeatPrice();
			chD = "work day";
		} else {
			th1 = new SeatPrice();
			th2 = new SeatPrice();
			th3 = new SeatPrice();
			chD = "Holiday";
		}

	}
	
	/*
	 * public int buyTicket(int time, char row, int col){ Seat seat = new
	 * Seat(time, row, col); if(!seats.contains(seat)){ seats.add(seat); return
	 * (int) DataSeatPrice.ticketPrices[((int) row)-64][col]; } else{ return 0;
	 * } }
	 */

}
